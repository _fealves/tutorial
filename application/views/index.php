<DOCTYPE=<!DOCTYPE html>
<html lang="PT-br">
<head>
	<title>Teste Programador - Cadastrar</title>
	<meta charset="UTF-8 ">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css">
	<script>
		$(function(){
			$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd',
	        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
	        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']});

		});

		$(function(){
  			$("#select").val($("#select option:first").val());
		});

	</script>
</head>
<body>
	<?php echo anchor('clienteController/buscar','Buscar')?>
	<form method="post">
		<h1></h1>
		<input type="text" name="nome" placeholder="Nome" required oninvalid="setCustomValidity('Por favor preencha este campo !')"/>
		<select class="select" id="select" name="select">
			<option value="">Estado </option>
			<option value="São Paulo - SP">São Paulo - SP</option>
			<option value="Rio de Janeiro - RJ">Rio de Janeiro - RJ</option>
			<option value="Minas Gerais - MG">Minas Gerais - MG</option>
			<input type="text" id="datepicker" placeholder="Data" name="data" required oninvalid="setCustomValidity('Por favor preencha este campo !')">
			<button type="submit">Cadastrar</button>
		</select>
	</form>
</body>
</html>
