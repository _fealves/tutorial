<DOCTYPE=<!DOCTYPE html>
<html lang="PT-br">
<head>
	<title>Teste Programador - Alterar</title>
	<meta charset="UTF-8 ">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css">
	<script>
		$(function() {
			$( "#datepicker" ).datepicker();
		});
	</script>
</head>
<body>
	<?php 
		echo anchor('clienteController/index','Home');
		
	
		$this->table->set_heading('Nome','Estado','Data');
		foreach ($resultado_busca as $res) {
			$this->table->add_row($res->nome,$res->estado,$res->data,anchor("clienteController/editar/$res->id_cadastro",'Editar'));
		}
		echo $this->table->generate();
	?>
</body>
</html>
