<DOCTYPE=<!DOCTYPE html>
<html lang="PT-br">
<head>
	<title>Teste Programador - Alterar</title>
	<meta charset="UTF-8 ">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css">
	<script>
		$(function(){
			$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd',
	        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
	        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']});

		});
		$(function(){
  			$("#select").val($("#select option:first").val());
		});
	</script>
</head>
<body>
	<?php echo anchor('clienteController/buscar','Buscar');


		foreach ($resultado_busca as $res) {


			$check0 = $check1 = $check2 = $check3 = '';
			switch ($res->estado) {
				case "Estado": {
				    $check0 = "selected";
				    break;
				}
				case "São Paulo - SP": {
				    $check1 = "selected";
				    break;
				}
				case "Rio de Janeiro - RJ": {
				    $check2 = "selected";
				    break;
				}
				case "Minas Gerais - MG": {
				    $check3 = "selected";
				    break;
				}
			}

			
			echo '<form method="post">';
				echo '<h1></h1>';
				echo '<input type="text" name="nome" placeholder="Nome" value='.$res->nome.' required oninvalid="setCustomValidity("Por favor preencha este campo !")"/>';
				echo '<select name="select" class="select">'; 
					echo '<option '.$check0.'>Estado</option>';
					echo '<option value="São Paulo - SP" '.$check1.'>São Paulo - SP</option>';
					echo '<option value="Rio de Janeiro - RJ" '.$check2.'>Rio de Janeiro - RJ</option>';
					echo '<option value="Minas Gerais - MG" '.$check3.'>Minas Gerais - MG</option>';
				echo '</select>';
				echo '<input type="text" id="datepicker" placeholder="Data" name="data" value='.$res->data.' required oninvalid="setCustomValidity("Por favor preencha este campo !">';
				echo '<button type="submit">Alterar</button>';
				
			echo '</form>';
		}
	?>
</body>
</html>
